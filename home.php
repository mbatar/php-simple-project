<?php
session_start();
include("getData.php");
if (isset($_POST["year"])){
    $_SESSION["islem"] = "Year";
    $_SESSION["year"] = $_POST["year"];
    header("Location:home.php");
}
if (isset($_POST["month"])){
    $_SESSION["islem"] = "Month";
    $_SESSION["month"] = $_POST["month"];
    $_SESSION["year"] = $_POST["yearForMonth"];
    header("Location:home.php");
}
if (isset($_POST["day"])){
    $_SESSION["islem"] = "Day";
    $_SESSION["day"] = $_POST["day"];
    $_SESSION["month"] = $_POST["monthForDay"];
    $_SESSION["year"] = $_POST["yearForDay"];
    header("Location:home.php");
}
if (isset($_POST["today"])){
    $_SESSION["islem"] = "Today";
    header("Location:home.php");
}
if (isset($_POST["all"])){
    $_SESSION["islem"] = "All";
    header("Location:home.php");
}



?>
<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Anasayfa</title>
</head>
<body class="bg-dark">
<?php include("header.php") ?>
<div class="container-fluid bg-light p-3 mt-3 rounded">
    <div class="table-responsive">
        <div class="mb-3 d-flex justify-content-between">
            <div>
                <h1>Günlük Hesap Listesi</h1>
            </div>
            <div class="d-flex justify-content-between " id="filterContent">
                <div class="form-group mr-2 align-self-center">
                    <select name="filter" id="filter" class="form-control">
                        <option value="joice">Filtre Seçenekleri</option>
                        <option value="allData">Genel</option>
                        <option value="onlyYear">Yıla Göre</option>
                        <option value="onlyMonth">Aya Göre</option>
                        <option value="onlyDay">Güne Göre</option>
                        <option value="today">Bugün</option>
                    </select>
                </div>
            </div>

        </div>
        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Bilgisayar Geliri</th>
                <th>Kasa Geliri</th>
                <th>Ayrı Gelir</th>
                <th>Toplam Gelir</th>
                <th>Gider Tutarı</th>
                <th>Günlük Ciro</th>
                <th>Tarih</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $toplam_ciro = 0;
            $toplam_gider = 0;
            $toplam_gelir = 0;
            $toplam_ayri_gelir = 0;
            $toplam_kasa_geliri = 0;
            $toplam_bilgisayar_geliri = 0;
            foreach ($getData as $data) {
                $toplam_ciro += $data["gunluk_toplam_ciro"];
                $toplam_gider += $data["gider_tutari"];
                $toplam_gelir += $data["toplam_gelir"];
                $toplam_ayri_gelir += $data["ayri_gelir"];
                $toplam_kasa_geliri += $data["kasa_geliri"];
                $toplam_bilgisayar_geliri += $data["bilgisayar_geliri"];
                ?>

                <tr>
                    <td><?php echo $data["bilgisayar_geliri"]; ?><span class="ml-3">₺</span></td>
                    <td><?php echo $data["kasa_geliri"]; ?><span class="ml-3">₺</span></td>
                    <td><?php echo $data["ayri_gelir"]; ?><span class="ml-3">₺</span></td>
                    <td><?php echo $data["toplam_gelir"]; ?><span class="ml-3">₺</span></td>
                    <td><?php echo $data["gider_tutari"]; ?><span class="ml-3">₺</span></td>
                    <td><?php echo ($data["gunluk_toplam_ciro"]) ? $data["gunluk_toplam_ciro"] : $data["toplam_gelir"]; ?>
                        <span class="ml-3">₺</span></td>
                    <td><?php echo $data["tarih"]; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="container-fluid bg-light p-3 mt-3 rounded">
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>Toplam Bilgisayar Geliri</th>
                <th>Toplam Kasa Geliri</th>
                <th>Toplam Ayrı Gelir</th>
                <th>Toplam Gelir</th>
                <th>Toplam Gider</th>
                <th>Toplam Ciro</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo number_format($toplam_bilgisayar_geliri,2,'.','.'); ?><span class="ml-3">₺</span></td>
                <td><?php echo number_format($toplam_kasa_geliri,2,'.','.')?><span class="ml-3">₺</span></td>
                <td><?php echo number_format($toplam_ayri_gelir,2,'.','.'); ?><span class="ml-3">₺</span></td>
                <td><?php echo number_format($toplam_gelir,2,'.','.'); ?><span class="ml-3">₺</span></td>
                <td><?php echo number_format($toplam_gider,2,'.','.'); ?><span class="ml-3">₺</span></td>
                <td><?php echo number_format($toplam_gelir - $toplam_gider,2,'.','.'); ?><span class="ml-3">₺</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="filtreOlustur.js"></script>
</body>
</html>
