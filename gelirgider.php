<?php
include("db.php");
session_start();


if (isset($_POST["bilgisayar_geliri"]) && isset($_POST["kasa_geliri"])){
    $bilgisayar_geliri = $_POST["bilgisayar_geliri"];
    $kasa_geliri = $_POST["kasa_geliri"];
    $user_id = $_SESSION["id"];
    if (empty($bilgisayar_geliri) || empty($kasa_geliri)){
        echo "Alanları boş bırakma";
    }else {
        $insert = $db -> query("INSERT INTO gelir_tbl SET bilgisayar_geliri=$bilgisayar_geliri,kasa_geliri=$kasa_geliri,user_id=$user_id ");
        $isOk = true;
    }
}

if (isset($_POST["gider_aciklama"]) && isset($_POST["gider_tutar"])){
    for ($i = 0; $i < count($_POST["gider_aciklama"]); $i++){
        $gider_aciklama = $_POST["gider_aciklama"][$i];
        $gider_tutar = $_POST["gider_tutar"][$i];
        $insert = $db -> prepare("INSERT INTO gider_tbl (gider_aciklama,tutar,user_id) VALUES (:gider_aciklama, :gider_tutar, :user_id) ");
        $insert -> bindParam(':gider_aciklama', $gider_aciklama);
        $insert -> bindParam(':gider_tutar', $gider_tutar);
        $insert -> bindParam(':user_id', $user_id);
        $insert -> execute();
    }
}

    header("Refresh:0; url=home.php");



?>