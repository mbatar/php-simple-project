var filteRList = document.getElementById('filter');
var filterContent = document.getElementById('filterContent');

created = false;
filteRList.addEventListener('click',()=>{
switch (filteRList.value) {
    case 'onlyYear':
        var node = document.getElementById("created");
        if (created == true && node.parentNode) {
            node.parentNode.removeChild(node);
        }
        createOnlyYearForm();
        break;
    case 'onlyMonth':
        var node = document.getElementById("created");
        if (created == true && node.parentNode) {
            node.parentNode.removeChild(node);
        }
        createOnlyMonthForm();
        break;
    case 'onlyDay':
        var node = document.getElementById("created");
        if (created == true && node.parentNode) {
            node.parentNode.removeChild(node);
        }
        createOnlyDayForm();
        break;
    case 'today':
        var node = document.getElementById("created");
        if (created == true && node.parentNode) {
            node.parentNode.removeChild(node);
        }
        createTodayForm();
        break;
    case "allData":
        var node = document.getElementById("created");
        if (created == true && node.parentNode) {
            node.parentNode.removeChild(node);
        }
        createAllDataForm();
        break;
    default:
        null
}
});
function createAllDataForm() {
    var form = document.createElement('form');
    form.setAttribute('action','home.php');
    form.setAttribute('method','post');
    form.setAttribute('id','created');

    var allDataFormGroup = document.createElement('div');
    allDataFormGroup.className = 'form-group d-flex justify-content-between';

    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('name','all');
    hiddenInput.setAttribute('id','all');
    hiddenInput.setAttribute('value','All');
    hiddenInput.setAttribute('type','hidden');

    var allDataFormSubmit = document.createElement('input');
    allDataFormSubmit.setAttribute('value','Listele');
    allDataFormSubmit.setAttribute('type','submit');
    allDataFormSubmit.setAttribute('name','listele');
    allDataFormSubmit.className = 'form-control ml-2 btn btn-primary';

    filterContent.appendChild(form);
    form.appendChild(allDataFormGroup);
    allDataFormGroup.appendChild(hiddenInput);
    allDataFormGroup.appendChild(allDataFormSubmit);

    created = true;
}

function createTodayForm() {
    var form = document.createElement('form');
    form.setAttribute('action','home.php');
    form.setAttribute('method','post');
    form.setAttribute('id','created');

    var todayFormGroup = document.createElement('div');
    todayFormGroup.className = 'form-group d-flex justify-content-between';

    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('name','today');
    hiddenInput.setAttribute('id','today');
    hiddenInput.setAttribute('value','Today');
    hiddenInput.setAttribute('type','hidden');

    var todayFormSubmit = document.createElement('input');
    todayFormSubmit.setAttribute('value','Listele');
    todayFormSubmit.setAttribute('type','submit');
    todayFormSubmit.setAttribute('name','listele');
    todayFormSubmit.className = 'form-control ml-2 btn btn-primary';

    filterContent.appendChild(form);
    form.appendChild(todayFormGroup);
    todayFormGroup.appendChild(hiddenInput);
    todayFormGroup.appendChild(todayFormSubmit);
    created = true
}

function createOnlyYearForm(){
    var form = document.createElement('form');
    form.setAttribute('action','home.php');
    form.setAttribute('method','post');
    form.setAttribute('id','created');

    var yearFormGroup = document.createElement('div');
    yearFormGroup.className = 'form-group d-flex justify-content-between';

    var yearFormInput = document.createElement('input');
    yearFormInput.className = 'form-control';
    yearFormInput.setAttribute('name','year');
    yearFormInput.setAttribute('id','year');
    yearFormInput.setAttribute('placeholder','Yıl Giriniz');

    var yearFormSubmit = document.createElement('input');
    yearFormSubmit.setAttribute('value','Listele');
    yearFormSubmit.setAttribute('type','submit');
    yearFormSubmit.setAttribute('name','listele');
    yearFormSubmit.className = 'form-control ml-2 btn btn-primary';

    filterContent.appendChild(form);
    form.appendChild(yearFormGroup);
    yearFormGroup.appendChild(yearFormInput);
    yearFormGroup.appendChild(yearFormSubmit);

    created = true;
}

function createOnlyMonthForm() {
    var form = document.createElement('form');
    form.setAttribute('action','home.php');
    form.setAttribute('method','post');
    form.setAttribute('id','created');


    var monthFormGroup = document.createElement('div');
    monthFormGroup.className = 'form-group d-flex justify-content-between';

    var monthFormInput = document.createElement('input');
    monthFormInput.className = 'form-control';
    monthFormInput.setAttribute('name','month');
    monthFormInput.setAttribute('id','month');
    monthFormInput.setAttribute('placeholder','Ay Giriniz - (Sayı)');

    var yearForMonthFormInput = document.createElement('input');
    yearForMonthFormInput.className = 'form-control';
    yearForMonthFormInput.setAttribute('name','yearForMonth');
    yearForMonthFormInput.setAttribute('id','yearForMonth');
    yearForMonthFormInput.setAttribute('placeholder','Yıl Giriniz');

    var monthFormSubmit = document.createElement('input');
    monthFormSubmit.setAttribute('value','Listele');
    monthFormSubmit.setAttribute('type','submit');
    monthFormSubmit.setAttribute('name','listele');
    monthFormSubmit.className = 'form-control ml-2 btn btn-primary';

    filterContent.appendChild(form);
    form.appendChild(monthFormGroup);
    monthFormGroup.appendChild(monthFormInput);
    monthFormGroup.appendChild(yearForMonthFormInput);
    monthFormGroup.appendChild(monthFormSubmit);

    created = true;

}

function createOnlyDayForm() {
    var form = document.createElement('form');
    form.setAttribute('action','home.php');
    form.setAttribute('method','post');
    form.setAttribute('id','created');


    var dayFormGroup = document.createElement('div');
    dayFormGroup.className = 'form-group d-flex justify-content-between';

    var dayFormInput = document.createElement('input');
    dayFormInput.className = 'form-control';
    dayFormInput.setAttribute('name','day');
    dayFormInput.setAttribute('id','day');
    dayFormInput.setAttribute('placeholder','Gün Giriniz - (Sayı)');

    var monthForDayFormInput = document.createElement('input');
    monthForDayFormInput.className = 'form-control';
    monthForDayFormInput.setAttribute('name','monthForDay');
    monthForDayFormInput.setAttribute('id','monthForDay');
    monthForDayFormInput.setAttribute('placeholder','Ay Giriniz - (Sayı)');

    var yearForDayFormInput = document.createElement('input');
    yearForDayFormInput.className = 'form-control';
    yearForDayFormInput.setAttribute('name','yearForDay');
    yearForDayFormInput.setAttribute('id','yearForDay');
    yearForDayFormInput.setAttribute('placeholder','Yıl Giriniz');

    var dayFormSubmit = document.createElement('input');
    dayFormSubmit.setAttribute('value','Listele');
    dayFormSubmit.setAttribute('type','submit');
    dayFormSubmit.setAttribute('name','listele');
    dayFormSubmit.className = 'form-control ml-2 btn btn-primary';

    filterContent.appendChild(form);
    form.appendChild(dayFormGroup);
    dayFormGroup.appendChild(dayFormInput);
    dayFormGroup.appendChild(monthForDayFormInput);
    dayFormGroup.appendChild(yearForDayFormInput);
    dayFormGroup.appendChild(dayFormSubmit);

    created = true;

}