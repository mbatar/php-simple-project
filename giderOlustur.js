var giderListe = document.getElementById('giderListe');
var button = document.getElementById('button');
var sayac = 1;
button.addEventListener('click',()=>{
    var formInline = document.createElement('div');
    formInline.className = 'form-inline  mt-3 mb-3 border-bottom p-3';

    var formGroupOne = document.createElement('div');
    formGroupOne.className = 'form-group mr-3';
    var formGroupOneLabel = document.createElement('label');
    formGroupOneLabel.textContent = 'Açıklama: ';
    formGroupOneLabel.setAttribute('for','gider_aciklama'+sayac);
    var formGroupOneInput = document.createElement('input');
    formGroupOneInput.setAttribute('name','gider_aciklama[]');
    formGroupOneInput.className = 'form-control';

    var formGroupTwo = document.createElement('div');
    formGroupTwo.className = 'form-group';
    var formGroupTwoLabel = document.createElement('label');
    formGroupTwoLabel.textContent = 'Tutar: ';
    formGroupTwoLabel.setAttribute('for','gider_tutar'+sayac);
    var formGroupTwoInput = document.createElement('input');
    formGroupTwoInput.setAttribute('name','gider_tutar[]');
    formGroupTwoInput.className = 'form-control';

    var deleteButton = document.createElement('div');
    deleteButton.className = 'btn btn-danger ml-5';
    deleteButton.textContent = 'SİL';


    giderListe.appendChild(formInline);
    formInline.appendChild(formGroupOne);
    formGroupOne.appendChild(formGroupOneLabel);
    formGroupOne.appendChild(formGroupOneInput);

    formInline.appendChild(formGroupTwo);
    formGroupTwo.appendChild(formGroupTwoLabel);
    formGroupTwo.appendChild(formGroupTwoInput);

    formInline.appendChild(deleteButton);

    deleteButton.addEventListener('click',(e)=>{
        e.target.parentElement.remove();
    })

    sayac++;
});