<?php
session_start();
include("getMoveData.php");
?>
<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Anasayfa</title>
</head>
<style>
    @media screen and (max-width: 800px) {
        .alt{
            border-top: 10px solid #343A40;
        }
    }
</style>
<body class="bg-dark">
<?php include("header.php") ?>
<div class="container-fluid bg-light p-2 mt-3 rounded">
    <div class="row">
        <div class="col-md-6 mt-3">
            <div class="table-responsive">
                <div class="mb-3">
                    <h1>Gelir Hareketleri</h1>
                </div>
                <table class="table table-bordered table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>Bilgisayar Geliri</th>
                        <th>Kasa Geliri</th>
                        <th>Ayrı Gelir</th>
                        <th>Toplam Gelir</th>
                        <th>Ekleyen</th>
                        <th>Tarih</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($getIncomeData as $data) {?>
                        <tr>
                            <td><?php echo $data["bilgisayar_geliri"]; ?><span class="ml-3">₺</span></td>
                            <td><?php echo $data["kasa_geliri"]; ?><span class="ml-3">₺</span></td>
                            <td><?php echo $data["ayri_gelir"]; ?><span class="ml-3">₺</span></td>
                            <td><?php echo $data["toplam_gelir"]; ?><span class="ml-3">₺</span></td>
                            <td><?php echo $data["ekleyen"]; ?><span class="ml-3"></span></td>
                            <td><?php echo $data["tarih"]; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6 border-left mt-3 alt">
            <div class="table-responsive">
                <div class="mb-3">
                    <h1>Gider Hareketleri</h1>
                </div>
                <table class="table table-bordered table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>Açıklama</th>
                        <th>Tutar</th>
                        <th>Ekleyen</th>
                        <th>Tarih</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($getOutgoingData as $data) {?>
                        <tr>
                            <td><?php echo $data["gider_aciklama"]; ?></td>
                            <td><?php echo $data["tutar"]; ?><span class="ml-3">₺</span></td>
                            <td><?php echo $data["ekleyen"]; ?></td>
                            <td><?php echo $data["tarih"]; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>
