<?php
session_start();
if (!isset($_SESSION["isLoggedIn"])){
    $_SESSION["isLoggedIn"] = false;
}else{
}
if (!isset($_SESSION["islem"])){
    $_SESSION["islem"] = "All";
}else{
}
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KD DENEME</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-dark">
<?php include("header.php"); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto bg-light shadow rounded mt-5">
            <div class="text-center p-3">
                <h1>GİRİŞ YAP</h1>
            </div>
            <div class="p-3">
                <form action="signInController.php" method="post">
                    <div class="form-group">
                        <label for="user_name">Kullanıcı Adı:</label>
                        <input type="text" id="user_name" name="user_name" placeholder="Kullanıcı adı giriniz" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="user_password">Kullanıcı Şifre:</label>
                        <input type="password" id="user_password" name="user_password" placeholder="Şifre giriniz" class="form-control">
                    </div>
                    <input type="submit" value="Giriş" class="btn btn-success mb-4 form-control">
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>