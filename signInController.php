<?php
session_start();
include("db.php");
$isOk = false;
$user_fullname = '';
if (isset($_POST["user_name"]) && isset($_POST["user_password"])){
    $user_name = $_POST["user_name"];
    $user_password = $_POST["user_password"];
    if (empty($user_name) || empty($user_password)){
        echo "Lütfen alanları boş bırakmayınız";
    }else{
        $users = $db -> query("SELECT * FROM users",PDO::FETCH_ASSOC);
        foreach ($users as $user){
            if ($user_name == $user["user_name"] && $user_password == $user["user_password"]){
                $_SESSION["id"] = $user["id"];
                $user_fullname = $user["name"];
                $isOk=true;
                break;
            }
        }
        if ($isOk){
            $_SESSION["isLoggedIn"] = true;
            $_SESSION["fullname"] = $user_fullname;
            header("Location:home.php");
        }elseif(!$isOk) {
            echo "Yanlış kullanıcı adı veya şifre";
        }
    }
}
?>
