<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo ($_SESSION["isLoggedIn"]) ? "home.php" : "index.php" ?>">Anasayfa</a>
            </li>
            <?php
            echo ($_SESSION["isLoggedIn"]) ? " <li class=\"nav-item dropdown\">
                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    İşlemler
                </a>
                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    <a class=\"dropdown-item\" href=\"gelirgiderekle.php\">Gelir/Gider Ekle</a>
                    <a class=\"dropdown-item\" href=\"hareketler.php\">Hareketleri Gör</a>

                    
                    <!--<div class=\"dropdown-divider\"></div>
                    <a class=\"dropdown-item\" href=\"#\">Toplam</a>
                    -->
                </div>
            </li>" : null;
            ?>
        </ul>
        <?php $name = (isset($_SESSION["fullname"])) ? $_SESSION["fullname"] : "" ; echo ($_SESSION["isLoggedIn"]) ? "<span class=\"my-2 my-lg-0\">
           <span class='mr-2'>$name</span> <a href='signOutController.php' class=\"btn btn-outline-danger my-2 my-sm-0\">Çıkış</a>
        </div>" : null ?>

    </div>
</nav>