<?php
include("db.php");

switch ($_SESSION["islem"]){
    case "All":
        $getData = $db->query("SELECT bilgisayar_geliri,kasa_geliri,ayri_gelir,toplam_gelir,
                                (SELECT SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gider_tutari,
                                (SELECT gelir_tbl.toplam_gelir - SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gunluk_toplam_ciro,
                                tarih FROM gelir_tbl GROUP BY tarih ORDER BY tarih DESC", PDO::FETCH_ASSOC);
        break;
    case "Year":
        $year = $_SESSION["year"];
        $getData = $db->query("SELECT bilgisayar_geliri,kasa_geliri,ayri_gelir,toplam_gelir,
                                (SELECT SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gider_tutari,
                                (SELECT gelir_tbl.toplam_gelir - SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gunluk_toplam_ciro,
                                tarih FROM gelir_tbl WHERE YEAR(tarih) = $year", PDO::FETCH_ASSOC);
        break;
    case "Month":
        $yearForMonth = $_SESSION["year"];
        $month = $_SESSION["month"];
        $getData = $db->query("SELECT bilgisayar_geliri,kasa_geliri,ayri_gelir,toplam_gelir,
                                (SELECT SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gider_tutari,
                                (SELECT gelir_tbl.toplam_gelir - SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gunluk_toplam_ciro,
                                tarih FROM gelir_tbl WHERE YEAR(tarih) = $yearForMonth AND MONTH(tarih)=$month", PDO::FETCH_ASSOC);
        break;
    case "Day":
        $yearForDay = $_SESSION["year"];
        $monthForDay = $_SESSION["month"];
        $day = $_SESSION["day"];
        $getData = $db->query("SELECT bilgisayar_geliri,kasa_geliri,ayri_gelir,toplam_gelir,
                                (SELECT SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gider_tutari,
                                (SELECT gelir_tbl.toplam_gelir - SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gunluk_toplam_ciro,
                                tarih FROM gelir_tbl WHERE DAY(tarih)=$day AND YEAR(tarih) = $yearForDay AND MONTH(tarih)=$monthForDay", PDO::FETCH_ASSOC);
        break;
    case "Today":
        $getData = $db->query("SELECT bilgisayar_geliri,kasa_geliri,ayri_gelir,toplam_gelir,
                                (SELECT SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gider_tutari,
                                (SELECT gelir_tbl.toplam_gelir - SUM(tutar) FROM gider_tbl WHERE tarih = gelir_tbl.tarih) AS gunluk_toplam_ciro,
                                tarih FROM gelir_tbl WHERE DAY(tarih) = DAY(CURDATE())", PDO::FETCH_ASSOC);
        break;
    default:
        null;
}

?>