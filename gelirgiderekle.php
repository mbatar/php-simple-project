<?php session_start(); ?>
<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Gelir - Gider Ekleme Sayfası</title>
</head>
<style>
    @media screen and (max-width: 800px) {
        .writem {
            width: 100% !important;
            margin-top: 10px;
        }

        .space {
            display: none !important;
        }
    }

</style>
<body class="bg-dark">
<?php include("header.php"); ?>
<div class="container  mt-5">
    <form action="gelirgider.php" method="post">
        <div class="bg-light rounded shadow p-3">
            <div class="text-center p-3 border-bottom mb-3">
                <h1>Gelir Bilgileri</h1>
            </div>

            <div class="form-group">
                <label for="bilgisayar_geliri">Bilgisayar Geliri</label>
                <input type="text" id="bilgisayar_geliri" name="bilgisayar_geliri" placeholder="bilgisayar geliri"
                       class="form-control">
            </div>
            <div class="form-group">
                <label for="kasa_geliri">Kasa Geliri</label>
                <input type="text" id="kasa_geliri" name="kasa_geliri" placeholder="kasa geliri" class="form-control">
            </div>
        </div>

        <div class="bg-light rounded shadow p-3 mt-3">
            <div class="text-center p-3 d-flex justify-content-between">
                <h1>Gider Bilgileri</h1>
                <div class="btn btn-primary align-self-center" id="button">Gider Oluştur</div>
            </div>
            <div id="giderListe">

            </div>
            <input type="submit" value="Ekle" class="btn btn-success form-control">
    </form>
</div>


</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="giderOlustur.js"></script>
</body>
</html>